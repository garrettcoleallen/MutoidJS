Crafty.c('TopBuildings', {
    required: "2D, Canvas, Collision, Color, Solid, spr_building, Scrolls",
    init: function(){
        this.color('#202020');
        this.x = 0;
        this.y = 0;
        this.w = 2100;
        this.h = 225;


        this.bind('Move', function(e){
            return function(){
                e.checkForRepeat();
            }
        }(this));
    },
    checkForRepeat: function(){
        if(this.x <= -1*this.w){
            
            buildings = Crafty('TopBuildings').get();
            for(var i =0; i < buildings.length; i++){
                buildings[i].destroy();
            }
            //build new one
            Crafty.e("TopBuildings");
            repeater = Crafty.e("TopBuildings");
            repeater.x = repeater.w;
        }

    }
});

Crafty.c('BottomRoad', {
    required: "2D, Canvas, spr_road, Scrolls",
    init: function(){
        this.x = 0;
        this.y = 226;
        this.w = 2100;
        this.h = 525;
        this.z=-1;


        this.bind('Move', function(e){
            return function(){
                e.checkForRepeat();
            }
        }(this));
    },
    checkForRepeat: function(){
        if(this.x <= -1*this.w){
            
            roads = Crafty('BottomRoad').get();
            for(var i =0; i < roads.length; i++){
                roads[i].destroy();
            }
            //build new one
            Crafty.e("BottomRoad");
            repeater = Crafty.e("BottomRoad");
            repeater.x = repeater.w;
        }

    }
});

Crafty.c('CarObject', {
    required: "2D, Canvas, Collision, Solid, spr_car, Scrolls",
    init: function(){
        this.collision([
           15,89,
           43,54,
           104,25,
           143,55,
           123,87,
           59,136,
           25,117

        ]);
    }
});

Crafty.c('BoxObject', {
    required: "2D, Canvas, Collision, Solid, spr_box, Scrolls",
    init: function(){
        this.collision([
            9,17,
            25,7,
            39,16,
            39,38,
            26,47,
            14,47
        ]);
    }
});

Crafty.c('SolidLeftPlayerOnly', {
    required: "2D, Canvas, Collision, Color",
    init: function(){
    }
});

Crafty.c('SolidRightPlayerOnly', {
    required: "2D, Canvas, Collision, Color",
    init: function(){
    }
});

Crafty.c('SolidBottomPlayerOnly', {
    required: "2D, Canvas, Collision, Color",
    init: function(){
    }
});

Crafty.c('BloodSpot', {
    required: "2D, Canvas, spr_bloodspot, Scrolls",
    init: function(){
        this.origin('center');
        
    }
});

Crafty.c('MoveBox', {
    required: "2D, Canvas, Collision, Color",
    init: function(){
        this.color('blue');
        this.alpha = 0;
        this.x = 825;
        this.y = 0;
        this.w = 225;
        this.h = 900;
    }
});

Crafty.c('TurboGun', {
    required: "2D, Canvas, Collision, SpriteAnimation, Scrolls, spr_turbogun",
    init: function(){
        this.onHit('PlayerCharacter', this.pickedUp);
    },
    pickedUp: function(hitDatas){
        hitData = hitDatas[0].obj;
        hitData.pickupTurboGun();
        this.destroy();
    }
});

Crafty.c('Medkit', {
    required: "2D, Canvas, Collision, SpriteAnimation, Scrolls, spr_medkit",
    init: function(){
        this.healAmount = 35;
        this.onHit('PlayerCharacter', this.pickedUp);
    },
    pickedUp: function(hitDatas){
        hitData = hitDatas[0].obj;
        hitData.takeHeal(this.healAmount);
        Crafty.audio.play('heal');
        this.destroy();
    }
});

